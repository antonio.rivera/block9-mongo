package com.example.block9mongodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Block9MongodbApplication {

	public static void main(String[] args) {
		SpringApplication.run(Block9MongodbApplication.class, args);
	}

}
