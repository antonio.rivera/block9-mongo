package com.example.block9mongodb.model;


import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "persona")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class Persona {

    @Id
    private Long id;

    private String nombre;

    private String apellido;


}
