package com.example.block9mongodb.controller;


import com.example.block9mongodb.model.Persona;
import com.example.block9mongodb.repository.PersonaRepository;
import com.example.block9mongodb.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.Optional;

@RestController
@RequiredArgsConstructor
public class PersonaController {


    private final PersonaService personaService;
    private final PersonaRepository personaRepository;

    /**
     * Crea una persona en la base de datos
     * @param persona
     * @return ResponseEntity<Persona>
     */
    @PostMapping("/crear")
    public ResponseEntity<Persona> crearPersona(@RequestBody Persona persona){
        return ResponseEntity.ok(personaService.crearPersona(persona));
    }

    /**
     * Elimina una persona de la base de datos
     *
     * @param id
     * @return
     */
    @DeleteMapping("/eliminar")
    public ResponseEntity<Optional<Persona>> eliminarPersona(@RequestParam Long id){
        return ResponseEntity.ok(personaService.eliminarPersona(id));
    }


    /**
     * Modifica una persona en la base de datos
     *
     * @param id
     * @param persona
     * @return
     */
    @PutMapping("/modificar")
    public ResponseEntity<Optional<Persona>> modificarPersona(@RequestParam Long id, @RequestBody Persona persona){
        return ResponseEntity.ok(personaService.modificarPersona(id, persona));
    }

    /**
     * Muestra las personas de la base de datos. Le he dado un numero determinado de 10 y solo mostrará la primera
     * página
     * @return
     */
    @GetMapping("/personas")
    public ResponseEntity<Page<Persona>> listarPersonas(){
        final Pageable pageable = PageRequest.of(0,10);
        return ResponseEntity.ok(personaRepository.findAll(pageable));
    }

    /**
     * Muestra una persona de la base de datos
     * @param id
     * @return
     */
    @GetMapping("/persona")
    public ResponseEntity<Optional<Persona>> mostrarPersona(@RequestParam Long id){
        Optional<Persona> persona = personaService.mostrarPersona(id);
        return ResponseEntity.ok(persona);
    }
}
