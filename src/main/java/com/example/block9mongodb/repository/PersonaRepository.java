package com.example.block9mongodb.repository;

import com.example.block9mongodb.model.Persona;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.stereotype.Repository;

//@Repository
//@EnableMongoRepositories
//@Primary
public interface PersonaRepository extends MongoRepository<Persona,Long> {
}
