package com.example.block9mongodb.service.Impl;

import com.example.block9mongodb.model.Persona;
import com.example.block9mongodb.repository.PersonaRepository;
import com.example.block9mongodb.service.PersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PersonaServiceImpl implements PersonaService {

    private final PersonaRepository personaRepository;

    @Override
    public Persona crearPersona(Persona persona) {
        return personaRepository.save(persona);
    }

    @Override
    public Optional<Persona> eliminarPersona(Long id) {
        Optional<Persona> persona = personaRepository.findById(id);
        if (personaRepository.findById(id).isPresent()){
            personaRepository.deleteById(id);
        }
        return persona;
    }

    @Override
    public Optional<Persona> mostrarPersona(Long id) {
        return personaRepository.findById(id);
    }

    @Override
    public List<Persona> listarPersonas() {
        return personaRepository.findAll();
    }

    @Override
    public Optional<Persona> modificarPersona(Long id, Persona persona) {
        Optional<Persona> persona1 = personaRepository.findById(id);
        if (personaRepository.findById(id).isPresent()){
            persona1.get().setNombre(persona.getNombre());
            persona1.get().setApellido(persona.getApellido());
            personaRepository.save(persona1.get());
        }
        return persona1;
    }
}
