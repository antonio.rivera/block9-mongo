package com.example.block9mongodb.service;


import com.example.block9mongodb.model.Persona;

import java.util.List;
import java.util.Optional;

public interface PersonaService {
    Persona crearPersona(Persona persona);
    Optional<Persona> eliminarPersona(Long id);
    Optional<Persona> mostrarPersona(Long id);
    List<Persona> listarPersonas();
    Optional<Persona> modificarPersona(Long id, Persona persona);
}
