package com.example.block9mongodb.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PersonaTest {

    @Test
    void getSetId() {
        Persona persona = new Persona();
        persona.setId(1L);
        assertEquals(1L,persona.getId());
    }
    @Test
    void noGetSetId() {
        Persona persona = new Persona();
        persona.setId(1L);
        assertNotEquals(2L,persona.getId());
    }

    @Test
    void getSetNombre() {
        Persona persona = new Persona();
        persona.setNombre("juan");
        assertEquals("juan",persona.getNombre());
    }
    @Test
    void noGetSetNombre() {
        Persona persona = new Persona();
        persona.setNombre("pepe");
        assertNotEquals("juan",persona.getNombre());
    }

    @Test
    void getSetApellido() {
        Persona persona = new Persona();
        persona.setApellido("lopez");
        assertEquals("lopez",persona.getApellido());
    }
    @Test
    void noGetSetApellido() {
        Persona persona = new Persona();
        persona.setApellido("lopez");
        assertNotEquals("ramirez",persona.getApellido());
    }


    @Test
    void testToString() {
        Persona persona = new Persona(1L,"juan","lopez");
        assertEquals("Persona(id=1, nombre=juan, apellido=lopez)",persona.toString());
    }
    @Test
    void noTestToString() {
        Persona persona = new Persona(1L,"juan","lopez");
        assertNotEquals("Persona(id=2, nombre=juan, apellido=lopez)",persona.toString());
    }

    @Test
    void builder() {
        Persona persona = Persona.builder()
                .id(1L)
                .nombre("juan")
                .apellido("lopez")
                .build();
        assertEquals(1L,persona.getId());
        assertEquals("juan",persona.getNombre());
        assertEquals("lopez",persona.getApellido());
    }
}