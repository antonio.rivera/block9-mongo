package com.example.block9mongodb.service.Impl;

import com.example.block9mongodb.model.Persona;
import com.example.block9mongodb.repository.PersonaRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class PersonaServiceImplTest {

    @Mock
    private PersonaRepository personaRepository;

    @InjectMocks
    private PersonaServiceImpl personaService;

    @Test
    void crearPersona() {
        Persona personaSimulada = new Persona(1L,"Antonio","Rivera");
        Persona personaEsperada = new Persona(1L,"Antonio","Rivera");
        Mockito.when(personaRepository.save(personaSimulada)).thenReturn(personaSimulada);
        final Persona result = personaService.crearPersona(personaSimulada);
        assertEquals(result.toString(),personaEsperada.toString());
        Mockito.verify(personaRepository).save(personaSimulada);
    }

    @Test
    void eliminarPersona() {
        Persona personaSimulada = new Persona(1L,"Antonio","Rivera");
        Persona personaEsperada = new Persona(1L,"Antonio","Rivera");
        Mockito.when(personaRepository.findById(1L)).thenReturn(Optional.of(personaSimulada));
        final Optional<Persona> result = personaService.eliminarPersona(1L);
        assertEquals(result.toString(),Optional.of(personaEsperada).toString());
        Mockito.verify(personaRepository).deleteById(1L);
    }

    @Test
    void mostrarPersona() {
        Persona personaSimulada = new Persona(1L,"Antonio","Rivera");
        Persona personaEsperada = new Persona(1L,"Antonio","Rivera");
        Mockito.when(personaRepository.findById(1L)).thenReturn(Optional.of(personaSimulada));
        final Optional<Persona> resultado = personaService.mostrarPersona(1L);
        assertEquals(resultado.toString(),Optional.of(personaEsperada).toString());
        Mockito.verify(personaRepository).findById(1L);
    }

    @Test
    void listarPersonas() {
        List<Persona> personaListSimulada = new ArrayList<>();
        personaListSimulada.add((new Persona(1L,"Antonio","Rivera")));
        personaListSimulada.add((new Persona(2L,"Antonio2","Rivera2")));


        List<Persona> personaListEsperada = new ArrayList<>();
        personaListEsperada.add((new Persona(1L,"Antonio","Rivera")));
        personaListEsperada.add((new Persona(2L,"Antonio2","Rivera2")));
        Mockito.when(personaRepository.findAll()).thenReturn(personaListSimulada);
        final List<Persona> resultado = personaService.listarPersonas();
        assertEquals(personaListEsperada.toString(),resultado.toString());
        Mockito.verify(personaRepository).findAll();

    }

    @Test
    void modificarPersona() {
        Persona personaSimulada = new Persona(1L,"Pepe","Lopez");
        Persona personaModificada = new Persona(1L,"Antonio","Rivera");
        Persona personaEsperada = new Persona(1L,"Antonio","Rivera");

        Mockito.when(personaRepository.findById(1L)).thenReturn(Optional.of(personaSimulada));
        final Optional<Persona> resultado = personaService.modificarPersona(1L,personaModificada);
        assertEquals(resultado.toString(),Optional.of(personaEsperada).toString());
    }
}